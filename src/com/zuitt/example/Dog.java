package com.zuitt.example;
//Child Class of Animal
    // "extends" keyword is used to inherit the properties and methods of the parent class.
public class Dog extends Animal {
    // properties
    private String breed;

    //constructor
    public Dog() {
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed) {
        super(name, color);
        this.breed = breed;
    }

    // setter and getter
    public String getBreed() {
        return this.breed;
    }

    public void setBreed() {
        this.breed = breed;
    }

    //method
    public void speak() {
        System.out.println("Woof woof!");
    }

    public void call() {
//        super.call();
        System.out.println("Hi my name is " + this.name + ", I am a dog");
    }
}
